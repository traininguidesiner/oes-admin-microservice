package com.oes.exception;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.oes.commons.BaseDto;

@RestControllerAdvice

public class OESAdvice {
	
	@Value("${general.exception.response}")
	protected String generalexceptioMessage;
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<BaseDto<Object>> generateException(final Exception exception) {		
		
		return new BaseDto<>(generalexceptioMessage, INTERNAL_SERVER_ERROR).respond();
	}
	
	@ExceptionHandler(OESException.class)
	public ResponseEntity<BaseDto<Object>> generateUserDefinedException(final OESException userDefinedException) {
		//if(!Objects.nonNull(userDefinedException.getException()))
			
		return new BaseDto<>(userDefinedException.getMessage(), userDefinedException.getStatus()).respond();
	}
	
	

}
