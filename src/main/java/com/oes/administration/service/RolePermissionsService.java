package com.oes.administration.service;

import java.util.List;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.model.RolePermissionsModel;

public interface RolePermissionsService {

	public RolePermissionsModel save(RolePermissionsModel rolePermission);
	
	public RolePermissionsModel update(RolePermissionsModel rolePermission);
	
	public void delete(RolePermissionsModel rolePermission);
	
	public RolePermissionsModel getByPermissionId(Integer rolePermissionId);
	
	public List<RolePermissionsModel> getAllPermissions(); 
	
	public List<RolePermissionsModel> getPermissionsListByRole(AdminRolesModel role); 
	
	public List<RolePermissionsModel> getPermissionsListByAdminId(AdminUserRegistrationModel admin);
	
}
