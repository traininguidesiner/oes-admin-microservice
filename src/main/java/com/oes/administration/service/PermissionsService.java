package com.oes.administration.service;

import java.util.List;

import com.oes.administration.model.PermissionsModel;

public interface PermissionsService {

	public PermissionsModel save(PermissionsModel permission);
	
	public PermissionsModel update(PermissionsModel permission);
	
	public void delete(PermissionsModel permission);
	
	public PermissionsModel getByPermissionId(Integer permissionId);
	
	public List<PermissionsModel> getAllPermissions(); 
}
