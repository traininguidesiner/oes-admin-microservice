package com.oes.administration.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.administration.model.PermissionsModel;
import com.oes.administration.repository.PermissionsRepository;
import com.oes.administration.service.PermissionsService;
import com.oes.exception.OESException;

@Service
public class PermissionsServiceImpl implements PermissionsService {

	@Autowired
	PermissionsRepository permissionsRepository;
	
	@Override
	public PermissionsModel save(PermissionsModel permission) {
		
		return permissionsRepository.save(permission);
	}

	@Override
	public PermissionsModel update(PermissionsModel permission) {
		
		if(Objects.isNull(permission.getPermissionId()))
		{
			throw new OESException("Bad Update Request", HttpStatus.BAD_REQUEST);
		}
		return permissionsRepository.save(permission);
	}

	@Override
	public void delete(PermissionsModel permission) {

		permissionsRepository.delete(permission);
	}

	@Override
	public PermissionsModel getByPermissionId(Integer permissionId) {
		
		return permissionsRepository.getOne(permissionId);
	}

	@Override
	public List<PermissionsModel> getAllPermissions() {
		
		return permissionsRepository.findAll();
	}

}
