package com.oes.administration.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.model.RolePermissionsModel;
import com.oes.administration.repository.AdminUserRegistrationRepository;
import com.oes.administration.repository.RolePermissionRepository;
import com.oes.administration.service.RolePermissionsService;
import com.oes.exception.OESException;

@Service
public class RolePermissionsServiceImpl implements RolePermissionsService {

	@Autowired
	RolePermissionRepository rolePermissionRepository;
	
	@Autowired
	AdminUserRegistrationRepository adminUserRegistrationRepository;
	@Override
	public RolePermissionsModel save(RolePermissionsModel permission) {
		
		return rolePermissionRepository.save(permission);
	}

	@Override
	public RolePermissionsModel update(RolePermissionsModel permission) {
		
		if(Objects.isNull(permission.getRolePermissionId()))
		{
			throw new OESException("Bad Update Request", HttpStatus.BAD_REQUEST);
		}
		return rolePermissionRepository.save(permission);
	}

	@Override
	public void delete(RolePermissionsModel permission) {

		rolePermissionRepository.delete(permission);
	}

	@Override
	public RolePermissionsModel getByPermissionId(Integer permissionId) {
		
		return rolePermissionRepository.getOne(permissionId);
	}

	@Override
	public List<RolePermissionsModel> getAllPermissions() {
		
		return rolePermissionRepository.findAll();
	}

	@Override
	public List<RolePermissionsModel> getPermissionsListByRole(AdminRolesModel role) {
		
		return rolePermissionRepository.findByRole(role);
	}

	@Override
	public List<RolePermissionsModel> getPermissionsListByAdminId(AdminUserRegistrationModel admin) {
		AdminUserRegistrationModel adminRes = adminUserRegistrationRepository.getOne(admin.getAdminId());
		return rolePermissionRepository.findByRole(adminRes.getRoleId());
	}

}
