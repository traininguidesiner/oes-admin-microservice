package com.oes.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="admin_user_details")
public class AdminUserRegistrationModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="admin_id")
	private Integer adminId;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="pwd")
	private String password;
	
	@Column(name="phone_no")
	private String phoneNo;
	
	@Column(name="user_type")
	private String userType;
	
	@Column(name="active_s")
	private Character activeStatus;
	
	@OneToOne
	@JoinColumn(name="role_id")
	private AdminRolesModel roleId;

	public Integer getAdminId() {
		return adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	

	public AdminRolesModel getRoleId() {
		return roleId;
	}

	public void setRoleId(AdminRolesModel roleId) {
		this.roleId = roleId;
	}

	

	public AdminUserRegistrationModel() {
		super();
	}

	public AdminUserRegistrationModel(Integer adminId, String firstName, String lastName, String email, String password,
			String phoneNo, String userType, Character activeStatus, AdminRolesModel roleId) {
		super();
		this.adminId = adminId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNo = phoneNo;
		this.userType = userType;
		this.activeStatus = activeStatus;
		this.roleId = roleId;
	}

	public Character getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Character activeStatus) {
		this.activeStatus = activeStatus;
	}

	
	
	
}
