package com.oes.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name="permissions")
@Data
public class PermissionsModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="permission_id")
	private Integer permissionId;
	
	@Column(name="permission")
	private String permission;
	
	@Column(name= "permission_description")
	private String permissionDescription;

	public Integer getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getPermissionDescription() {
		return permissionDescription;
	}

	public void setPermissionDescription(String permissionDescription) {
		this.permissionDescription = permissionDescription;
	}

	public PermissionsModel(Integer permissionId, String permission, String permissionDescription) {
		super();
		this.permissionId = permissionId;
		this.permission = permission;
		this.permissionDescription = permissionDescription;
	}

	public PermissionsModel() {
		super();
	}
	
	
}
