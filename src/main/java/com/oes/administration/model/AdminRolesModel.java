package com.oes.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="admin_roles")
public class AdminRolesModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="admin_role_id")
	private Integer adminRoleId;
	
	@Column(name="role_name")
	private String adminRoleName;

	public Integer getAdminRoleId() {
		return adminRoleId;
	}

	public void setAdminRoleId(Integer adminRoleId) {
		this.adminRoleId = adminRoleId;
	}

	public String getAdminRoleName() {
		return adminRoleName;
	}

	public void setAdminRoleName(String adminRoleName) {
		this.adminRoleName = adminRoleName;
	}

	public AdminRolesModel(Integer adminRoleId, String adminRoleName) {
		super();
		this.adminRoleId = adminRoleId;
		this.adminRoleName = adminRoleName;
	}

	public AdminRolesModel() {
		super();
	}
	
	

}
