package com.oes.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name="role_permissions")
@Data
public class RolePermissionsModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="role_permission_id")
	private Integer rolePermissionId;
	
	@OneToOne
	@JoinColumn(name="role_id")
	private AdminRolesModel role;
	
	@OneToOne
	@JoinColumn(name="permission_id")
	private PermissionsModel permission;
	
	@Column(name="active_s")
	private Character activeS;

	public Integer getRolePermissionId() {
		return rolePermissionId;
	}

	public void setRolePermissionId(Integer rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}

	public AdminRolesModel getRole() {
		return role;
	}

	public void setRole(AdminRolesModel role) {
		this.role = role;
	}

	public PermissionsModel getPermission() {
		return permission;
	}

	public void setPermission(PermissionsModel permission) {
		this.permission = permission;
	}

	public Character getActiveS() {
		return activeS;
	}

	public void setActiveS(Character activeS) {
		this.activeS = activeS;
	}

	public RolePermissionsModel(Integer rolePermissionId, AdminRolesModel role, PermissionsModel permission,
			Character activeS) {
		super();
		this.rolePermissionId = rolePermissionId;
		this.role = role;
		this.permission = permission;
		this.activeS = activeS;
	}

	public RolePermissionsModel() {
		super();
	}

}
