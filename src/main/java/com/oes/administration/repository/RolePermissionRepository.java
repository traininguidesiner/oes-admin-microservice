package com.oes.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.model.RolePermissionsModel;

public interface RolePermissionRepository extends JpaRepository<RolePermissionsModel, Integer> {

	List<RolePermissionsModel> findByRole(AdminRolesModel role);

}
