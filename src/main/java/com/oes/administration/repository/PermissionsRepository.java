package com.oes.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oes.administration.model.PermissionsModel;

public interface PermissionsRepository extends JpaRepository<PermissionsModel, Integer> {

}
