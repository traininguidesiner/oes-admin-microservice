package com.oes.administration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.service.AdminRolesService;
import com.oes.commons.BaseDto;

@RestController
@CrossOrigin
public class AdminRolesController 
{
	@Autowired
	AdminRolesService service;
	
	@PostMapping("/EnterAdminRole")
	public ResponseEntity<BaseDto<AdminRolesModel>> enterAdmin(@RequestBody AdminRolesModel adminDetails)
	{
		AdminRolesModel adminInfo = service.saveAdminRole(adminDetails); 
		return new BaseDto(adminInfo, "AdminRole Entry Successfull", HttpStatus.OK).respond();
	}
	
	@PostMapping("/UpdateAdminRole")
	public ResponseEntity<BaseDto<AdminRolesModel>> updateAdmin(@RequestBody AdminRolesModel adminDetails)
	{
		AdminRolesModel adminInfo = service.updateAdminRole(adminDetails);
		return new BaseDto(adminInfo, "AdminRole Update Successfull", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/DeleteAdminRoles")
	public ResponseEntity<BaseDto<AdminRolesModel>> delete(@RequestBody AdminRolesModel adminDetails)
	{
		service.deleteAdminRole(adminDetails);
		return new BaseDto("AdminRole delete Successfull", HttpStatus.OK).respond();
		
	}
	
	@GetMapping("/FindAdminRoleById")
	public ResponseEntity<BaseDto<AdminRolesModel>> findByUserId(@RequestParam Integer roleId)
	{
		AdminRolesModel adminInfo = service.findAdminRoleById(roleId);
		return new BaseDto(adminInfo, "AdminRole finding Successfull", HttpStatus.OK).respond();
	}

	@GetMapping("/FindAdminRoleByName")
	public ResponseEntity<BaseDto<AdminRolesModel>> findByUserName(@RequestParam String name)
	{
		AdminRolesModel adminInfo = service.findAdminRoleByName(name);
		return new BaseDto(adminInfo, "AdminRole finding Successfull", HttpStatus.OK).respond();
	}
	
	@GetMapping("/GetAllAdminRole")
	public ResponseEntity<BaseDto<List<AdminRolesModel>>> getAllAdminRole()
	{
		List<AdminRolesModel> adminInfo = service.getAllAdminRoles();
		return new BaseDto(adminInfo, "AdminRole finding Successfull", HttpStatus.OK).respond();
	}
	
		
	//@GetMapping("/CheckAdminRoleName")
	//public boolean checkAdminRoleName(@RequestParam String uName)
	//{
	//	return service.getAll(uName);
	//}

}
