package com.oes.administration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.administration.model.AdminRolesModel;
import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.model.RolePermissionsModel;
import com.oes.administration.service.RolePermissionsService;
import com.oes.commons.BaseDto;

@RestController
@CrossOrigin
public class RolePermissionsController {

	@Autowired
	RolePermissionsService rolePermissionsService;
	
	@PostMapping("/saveRolePermission")
	public ResponseEntity<BaseDto<RolePermissionsModel>> savePermission(@RequestBody RolePermissionsModel rolePermission){
		
		RolePermissionsModel rolePermissionRes = rolePermissionsService.save(rolePermission);
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Saved Successfuly", HttpStatus.OK).respond();
	}
	
	@PutMapping("/updateRolePermission")
	public ResponseEntity<BaseDto<RolePermissionsModel>> updatePermission( @RequestBody RolePermissionsModel rolePermission){
		
		RolePermissionsModel rolePermissionRes = rolePermissionsService.update(rolePermission);
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Updated Successfuly", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/deleteRolePermission")
	public ResponseEntity<BaseDto<Object>> deletePermission(@RequestBody RolePermissionsModel rolePermission){
		
		rolePermissionsService.delete(rolePermission);
		
		return new BaseDto<>("RolePrmission Deleted Successfuly", HttpStatus.OK).respond();
	}
	
	@GetMapping("/getRolePermissionById")
	public ResponseEntity<BaseDto<RolePermissionsModel>> getPermissionById(@RequestParam Integer rolePermissionId){
		
		RolePermissionsModel rolePermissionRes = rolePermissionsService.getByPermissionId(rolePermissionId);
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Retrived Successfuly", HttpStatus.OK).respond();
	}
	
	@GetMapping("/getAllRolePermissions")
	public ResponseEntity<BaseDto<List<RolePermissionsModel>>> getAllPermission(){
		
		List<RolePermissionsModel> rolePermissionRes = rolePermissionsService.getAllPermissions();
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Retrived Successfuly", HttpStatus.OK).respond();
	}
	
	@PostMapping("/getRolePermissionsByRole")
	public ResponseEntity<BaseDto<List<RolePermissionsModel>>> getPermissionsListByRole(@RequestBody AdminRolesModel role){
		
		List<RolePermissionsModel> rolePermissionRes = rolePermissionsService.getPermissionsListByRole(role);
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Retrived Successfuly", HttpStatus.OK).respond();
	}
	
	@PostMapping("/getRolePermissionsByAdminId")
	public ResponseEntity<BaseDto<List<RolePermissionsModel>>> getPermissionsListByAdminId(@RequestBody AdminUserRegistrationModel admin){
		
		List<RolePermissionsModel> rolePermissionRes = rolePermissionsService.getPermissionsListByAdminId(admin);
		
		return new BaseDto<>(rolePermissionRes,"RolePrmission Retrived Successfuly", HttpStatus.OK).respond();
	}
}
