package com.oes.administration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.service.AdminUserRegistrationService;
import com.oes.commons.BaseDto;


@RestController
@CrossOrigin
public class AdminUserRegistrationController 
{
	@Autowired
	AdminUserRegistrationService service;
	
	@PostMapping("/EnterAdmin")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> enterAdmin(@RequestBody AdminUserRegistrationModel adminDetails)
	{
		AdminUserRegistrationModel adminInfo = service.saveAdmin(adminDetails); 
		return new BaseDto(adminInfo, "Admin Entry Successfull", HttpStatus.OK).respond();
	}
	
	@PutMapping("/UpdateAdmin")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> updateAdmin(@RequestBody AdminUserRegistrationModel adminDetails)
	{
		AdminUserRegistrationModel adminInfo = service.updateAdmin(adminDetails);
		return new BaseDto(adminInfo, "Admin Update Successfull", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/DeleteAdmin")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> delete(@RequestBody AdminUserRegistrationModel adminDetails)
	{
		service.deleteAdmin(adminDetails);
		return new BaseDto("Admin delete Successfull", HttpStatus.OK).respond();
		
	}
	
	@GetMapping("/FindAdminById")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> findByUserId(@RequestParam Integer userId)
	{
		AdminUserRegistrationModel adminInfo = service.findAdminById(userId);
		return new BaseDto(adminInfo, "Admin finding Successfull", HttpStatus.OK).respond();
	}

	@GetMapping("/FindAdminByUname")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> findByUserName(@RequestParam String userName)
	{
		AdminUserRegistrationModel adminInfo = service.findAdminByName(userName);
		return new BaseDto(adminInfo, "Admin finding Successfull", HttpStatus.OK).respond();
	}
	
	@GetMapping("/FindAdminByUsrAndPwd")
	public ResponseEntity<BaseDto<AdminUserRegistrationModel>> findByUnameAndPwd(@RequestParam String uName, @RequestParam String password)
	{
		AdminUserRegistrationModel adminInfo = service.findAdminByEmailAndPwd(uName, password);
		return new BaseDto(adminInfo, "Admin finding Successfull", HttpStatus.OK).respond();
	}
	
	@GetMapping("/CheckAdminName")
	public boolean checkAdminName(@RequestParam String uName)
	{
		return service.getAll(uName);
	}
	

	@GetMapping("/GetAllAdminUser")
	public ResponseEntity<BaseDto<List<AdminUserRegistrationModel>>> getAllAdminUser()
	{
		List<AdminUserRegistrationModel> adminInfo = service.getAllAdminUser();
		return new BaseDto(adminInfo, "Admin finding Successfull", HttpStatus.OK).respond();
		
	}

	@GetMapping("/updateAdminRoleByAdminId")
	public ResponseEntity<BaseDto<Object>> updateAdminRole(@RequestParam Integer roleId, @RequestParam Integer adminId)
	{
		service.updateAdminRole(roleId, adminId);
		return new BaseDto("Admin Role Update Successfull", HttpStatus.OK).respond();

	}

}
