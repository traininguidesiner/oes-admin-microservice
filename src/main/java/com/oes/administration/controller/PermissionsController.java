package com.oes.administration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.administration.model.PermissionsModel;
import com.oes.administration.service.PermissionsService;
import com.oes.commons.BaseDto;

@RestController
@CrossOrigin
public class PermissionsController {

	@Autowired
	PermissionsService permissionsService;
	
	@PostMapping("/savePermission")
	public ResponseEntity<BaseDto<PermissionsModel>> savePermission(@RequestBody PermissionsModel permission){
		
		PermissionsModel permissionRes = permissionsService.save(permission);
		
		return new BaseDto<>(permissionRes,"Prmission Saved Successfuly", HttpStatus.OK).respond();
	}
	
	@PutMapping("/updatePermission")
	public ResponseEntity<BaseDto<PermissionsModel>> updatePermission( @RequestBody PermissionsModel permission){
		
		PermissionsModel permissionRes = permissionsService.update(permission);
		
		return new BaseDto<>(permissionRes,"Prmission Updated Successfuly", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/deletePermission")
	public ResponseEntity<BaseDto<Object>> deletePermission(@RequestBody PermissionsModel permission){
		
		permissionsService.delete(permission);
		
		return new BaseDto<>("Prmission Deleted Successfuly", HttpStatus.OK).respond();
	}
	
	@GetMapping("/getPermissionById")
	public ResponseEntity<BaseDto<PermissionsModel>> getPermissionById(@RequestParam Integer permissionId){
		
		PermissionsModel permissionRes = permissionsService.getByPermissionId(permissionId);
		
		return new BaseDto<>(permissionRes,"Prmission Retrived Successfuly", HttpStatus.OK).respond();
	}
	
	@GetMapping("/getAllPermission")
	public ResponseEntity<BaseDto<List<PermissionsModel>>> getAllPermission(){
		
		List<PermissionsModel> permissionRes = permissionsService.getAllPermissions();
		
		return new BaseDto<>(permissionRes,"Prmission Retrived Successfuly", HttpStatus.OK).respond();
	}
}
