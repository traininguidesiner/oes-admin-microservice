package com.oes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OesAdminMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OesAdminMicroserviceApplication.class, args);
	}

}
